
const express = require('express');
const app = express();
const router = express.Router();
const path = require('path');
const fs = require('fs');

import React from 'react';
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import routes from './src/routes';

// PARSER
let bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// STATIC CONTENT
app.use(express.static(path.join(__dirname, './public')));


// REACT ROUTER
app.get('*', (req, res) => {
    match({ routes: routes, location: req.url }, (err, redirect, props) => {
        if (err) {
            res.status(500).send(err.message)
        } else if (redirect) {
            res.redirect(redirect.pathname + redirect.search)
        } else if (props) {
            const appHtml = renderToString(<RouterContext {...props}/>);
            res.send(renderPage(appHtml))
        } else {
            res.status(404).send('Not Found')
        }
    })
});
function renderPage(appHtml) {
    return `
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tribal</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body>
    <div id=app>${appHtml}</div>
    <script src="./bundle.js"></script>
    </body>
    </html>
    `
}
//ROUTER FUNCTIONS
router.use(function(req, res, next) {
    console.log('Server ' + req.method, req.url, req.body);
    next();
});

//SERVER CONNECTION
var port = process.env.PORT || 7373;
var server = app.listen(process.env.PORT || port || 7373, function() {
    console.log('Server is running at https://localhost:' +
        server.address().port);
});