
const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function(x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function(mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });

module.exports = {
    watch: false,
    entry: ["babel-polyfill", path.resolve(__dirname, './src/app.js')],
    output: {
        path: path.resolve(__dirname, './public'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.html$/,
                loader: 'file-loader?name=/public/[name].[ext]',
            },
            {
                test: /\.(js|jsx)$/,
                exclude: path.resolve(__dirname, 'node_modules'),
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react', 'stage-0'],
                },
            },
            {
                test: /\.json$/,
                loader: "json-loader"
            },
            {
                test: /\.(json|map.json)$/,
                exclude: path.resolve(__dirname, 'node_modules'),
                loader: ['json-loader', 'json'],
            },
        ]
    },
    node: {
        fs: "empty",
        __filename: true,
        __dirname: true,
        global: true,
        process: true,
        console: true,
    },
    externals: {
        'path': '{}',
        "fs": "{}",
        nodeModules,
    },
    plugins: [
        new CopyWebpackPlugin([
            {from: './src/index.html'},
            {from: './src/app.js'},
            {from: './src/routes.js'},
            {from: './src/pages', to: './pages'},
            {from: './src/static', to: './static'},
        ]),
        new webpack.optimize.AggressiveMergingPlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production"),
            },
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                conditionals: true,
                loops: true,
                unused: true,
                join_vars: true,
                drop_console: true,
            },
            mangle: true,
            sourcemap: false,
            beautify: false,
            dead_code: true,
        }),
    ],
    stats: {
        colors: true,
    },
    devtool: 'cheap-module-source-map',
};