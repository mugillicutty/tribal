/**
 * Created by jwarnock on 7/11/17.
 */
const fs = require('fs');
const path = require('path');
const axios = require('axios');

const users = {

    getUsers: (numberOfUsers) => {
        return new Promise((resolve, reject) => {
            if (!numberOfUsers) {
                reject(new Error('Parameters needed'));
            } else if (numberOfUsers.constructor !== Number) {
                reject(new Error('Parameters in wrong format'));
            } else {
                numberOfUsers.toString();
                return axios.get('https://randomuser.me/api/?results=' + numberOfUsers)
                    .then(res => {
                        resolve(res);
                    })
            }
        })
    },

    indexUsers: (list, nameSelection) => {
        return new Promise((resolve, reject) => {
            if ((!list) || (!nameSelection)) {
                reject(new Error('Parameters needed'));
            } else if ((list.constructor !== Array) || (nameSelection.constructor !== String)) {
                reject(new Error('Parameters in wrong format'));
            } else {
                let indexedUsers = [];
                list.forEach(user => {
                    const beginLetter = {beginLetter: user.name[nameSelection].slice(0, 1).toUpperCase()};
                    const indexedUser = Object.assign(beginLetter, user);
                    indexedUsers.push(indexedUser);
                });
                resolve(indexedUsers);
            }
        })
    },

    localeAlphabetizeUsers: (list, nameSelection) => {
        let tempList = list;
        return new Promise((resolve, reject) => {
            if (!list) {
                reject(new Error('Parameters needed'));
            } else if (list.constructor !== Array) {
                reject(new Error('Parameters in wrong format'));
            } else {
                tempList.sort((a, b) => {
                    return a.name[nameSelection].localeCompare(b.name[nameSelection]);
                });
                resolve(tempList);
            }
        })
    },

    groupUsers: (list) => {
        let tempList = list;
        return new Promise((resolve, reject) => {
            if (!list) {
                reject(new Error('Parameters needed'));
            } else if (list.constructor !== Array) {
                reject(new Error('Parameters in wrong format'));
            } else {
                const groupedUsers = tempList.reduce(function(obj,item){
                    obj[item.beginLetter] = obj[item.beginLetter] || [];
                    obj[item.beginLetter].push(item);
                    return obj;
                }, {});

                const usersGroups =  Object.keys(groupedUsers).map(function(key){
                    return {group: key, groupedUsers: groupedUsers[key]};
                });
                resolve(usersGroups);
            }
        })
    },

    filterList: (list, userString, nameSelection) => {
        let tempList = list;
        return new Promise((resolve, reject) => {
            if (!list) {
                reject(new Error('Parameters needed'));
            } else if (list.constructor !== Array) {
                reject(new Error('Parameters in wrong format'));
            } else {
                let foundUsers = [];
                if (!userString) {
                    foundUsers = tempList
                } else {
                    tempList.forEach(user => {
                        if ((user.name[nameSelection].indexOf(userString) !== -1)) {
                            const userIndex = {userIndex: user.name[nameSelection].indexOf(userString)};
                            const indexedUser = Object.assign(userIndex, user);
                            foundUsers.push(indexedUser);
                        }
                    });
                }
                resolve(foundUsers);
            }
        })
    },

    alphabetizeGroups: (list) => {
        return new Promise((resolve, reject) => {
            if (!list) {
                reject(new Error('Parameters needed'));
            } else if (list.constructor !== Array) {
                reject(new Error('Parameters in wrong format'));
            } else {
                list.sort((a, b) => {
                    return +(a.group > b.group) || +(a.group === b.group) - 1;
                });
                resolve(list);
            }
        })
    },

};

export default users;