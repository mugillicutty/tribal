/**
 * Created by jwarnock on 7/19/17.
 */
import {combineReducers} from 'redux';
import users from './users';
import indexedUsers from './users';
import alphaUsers from './users';
import groupedUsers from './users';
import filteredUsers from './users';
import alphaGroups from './users';

export default combineReducers({
    users,
    indexedUsers,
    alphaUsers,
    groupedUsers,
    filteredUsers,
    alphaGroups
});