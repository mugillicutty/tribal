/**
 * Created by jwarnock on 7/19/17.
 */

import {
    SET_USERS,
    INDEX_USERS,
    LOCALE_ALPHABETIZE_USERS,
    GROUP_USERS,
    FILTER_USERS,
    ALPHABETIZE_GROUPS,
    // CURRENT_USERS,
} from './actions';

const initialState = [];

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_USERS:
            return action.users;
        case INDEX_USERS:
            return action.indexedUsers;
        case LOCALE_ALPHABETIZE_USERS:
            return action.alphaUsers;
        case GROUP_USERS:
            return action.groupedUsers;
        case FILTER_USERS:
            return action.filteredUsers;
        case ALPHABETIZE_GROUPS:
            return action.alphaGroups;
        default:
            return state;
    }
};