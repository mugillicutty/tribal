/**
 * Created by jwarnock on 7/19/17.
 */

import UserFunc from '../../users/UsersFunctions';

export const SET_USERS = 'SET_USERS';
export function getUsers(userNumber) {
    return dispatch =>
    UserFunc.getUsers(userNumber)
        .then(res =>
            dispatch(setUsers(res.data.results))
        );
}
export function setUsers(users) {
    return Object.freeze(Object.assign({}, {
        type: SET_USERS,
        users,
    }));
}

export const INDEX_USERS = 'INDEX_USERS';
export function indexTheUsers(users, nameSelection) {
    return dispatch =>
    UserFunc.indexUsers(users, nameSelection)
        .then(res => dispatch(setIndexedUsers(res)))
}
export function setIndexedUsers(indexedUsers) {
    return Object.freeze(Object.assign({}, {
        type: INDEX_USERS,
        indexedUsers,
    }));
}

export const LOCALE_ALPHABETIZE_USERS = 'LOCALE_ALPHABETIZE_USERS';
export function localeAlphabetizeUsers(alphaList, nameSelection) {
    return dispatch =>
    UserFunc.localeAlphabetizeUsers(alphaList, nameSelection)
        .then(res => dispatch(setAlphaUsers(res)))
}
export function setAlphaUsers(alphaUsers) {
    return Object.freeze(Object.assign({}, {
        type: LOCALE_ALPHABETIZE_USERS,
        alphaUsers
    }));
}

export const GROUP_USERS = 'GROUP_USERS';
export function groupUsers(groupedUsers) {
    return dispatch =>
    UserFunc.groupUsers(groupedUsers)
        .then(res => dispatch(setUserGroups(res)))
}
export function setUserGroups(groupedUsers) {
    return Object.freeze(Object.assign({}, {
        type: GROUP_USERS,
        groupedUsers
    }));
}

export const FILTER_USERS = 'FILTER_USERS';
export function filterUsers(filteredUsers, userString, nameSelection) {
    return dispatch =>
    UserFunc.filterList(filteredUsers, userString, nameSelection)
        .then(res => dispatch(setFilteredGroups(res)))
}
export function setFilteredGroups(filteredUsers) {
    return Object.freeze(Object.assign({}, {
        type: FILTER_USERS,
        filteredUsers
    }));
}

export const ALPHABETIZE_GROUPS = 'ALPHABETIZE_GROUPS';
export function alphabetizeGroups(groupList) {
    return dispatch =>
        UserFunc.alphabetizeGroups(groupList)
            .then(res => dispatch(setAlphaGroups(res)))
}
export function setAlphaGroups(alphaGroups) {
    return Object.freeze(Object.assign({}, {
        type: ALPHABETIZE_GROUPS,
        alphaGroups
    }));
}