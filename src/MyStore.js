
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducer';

const Store = createStore(reducer, compose(
    applyMiddleware(thunk)
));

export default Store;