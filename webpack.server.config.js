
let webpack = require('webpack');
let fs = require('fs');
let path = require('path');

let webpackServerConfig = {
    watch: false,
    entry: path.resolve(__dirname, './server.js'),
    output: {
        path: path.resolve(__dirname),
        filename: './server.bundle.js',
    },
    target: 'node',
    externals: fs.readdirSync(path.resolve(__dirname, 'node_modules')).concat([
        'react-dom/server', 'react/addons',
    ]).reduce(function (ext, mod) {
        ext[mod] = 'commonjs ' + mod;
        return ext
    }, {}),
    node: {
        fs: "empty",
        __filename: true,
        __dirname: true,
        global: true,
        process: true,
        console: true,
    },
    module: {
        loaders: [
            // {
            //     test: /\.html$/,
            //     loader: 'file-loader?name=[name].[ext]',
            // },
            {
                test: /\.(js|jsx)$/,
                exclude: [path.resolve(__dirname, 'node_modules')],
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react', 'stage-0'],
                },
            },
            {
                test: /\.json$/,
                loader: "json-loader",
            },
            {
                test: /\.(json|map.json)$/,
                exclude: path.resolve(__dirname, 'node_modules'),
                loader: ['json-loader', 'json'],
            }
        ]
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            },
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                conditionals: true,
                loops: true,
                unused: true,
                join_vars: true,
                drop_console: true
            },
            mangle: true,
            sourcemap: false,
            beautify: false,
            dead_code: true
        }),
    ],
    devtool: 'cheap-module-source-map',
};

module.exports = webpackServerConfig;