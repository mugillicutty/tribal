/**
 * Created by jwarnock on 7/11/17.
 */
import React from 'react';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import {GridList, GridTile} from 'material-ui/GridList';
import Divider from 'material-ui/Divider';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import Store from '../MyStore';

// REDUCERS
import {
    getUsers,
    indexTheUsers,
    localeAlphabetizeUsers,
    groupUsers,
    alphabetizeGroups,
    filterUsers
} from '../reducer/users/actions';


import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
const UserSortContent = () => (
    <MuiThemeProvider>
        <UserSortCard/>
    </MuiThemeProvider>
);
export default class UserSortPage extends React.Component {
    render() {
        return (
            <UserSortContent/>
        )
    }
}


export class UserSortCard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            numberOfUsers: 50,
            loadList: [],
            usersName: '',
            userList: [],
            nameSearchSelection: 'last',
            filteredList: [],
            alphaList: [],
            lastChecked: true,
            firstChecked: false,
            userState: [],
            profileOpen: false,
        };
    };

    dispatchUsers = (userNumber) => {
        return Store.dispatch(getUsers(userNumber))
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log(err);
                return err;
            })
    };
    dispatchIndex = (userList, nameSelection) => {
        return Store.dispatch(indexTheUsers(userList, nameSelection))
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log(err);
                return err;
            })
    };
    dispatchAlphabetizeUsers = (userList, nameSelection) => {
        return Store.dispatch(localeAlphabetizeUsers(userList, nameSelection))
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log(err);
                return err;
            })
    };
    dispatchGrouping = (filteredUsers) => {
        return Store.dispatch(groupUsers(filteredUsers))
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log(err);
                return err;
            })
    };
    dispatchAlphabetizeGroups = (groupedList) => {
        return Store.dispatch(alphabetizeGroups(groupedList))
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log(err);
                return err;
            })
    };
    dispatchFilterUsers = (list, userString, nameSelection) => {
        return Store.dispatch(filterUsers(list, userString, nameSelection))
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log(err);
                return err;
            })
    };

    componentDidMount() {
        const userNumber = this.state.numberOfUsers;
        this.dispatchUsers(userNumber)
            .then(res => {
                console.log(res);
                const userList = res.users;
                this.setState({userList: res.users});
                const nameSelection = this.state.nameSearchSelection;
                return this.dispatchIndex(userList, nameSelection);
            })
            .then(res => {
                console.log(res);
                const nameSelection = this.state.nameSearchSelection;
                return this.dispatchAlphabetizeUsers(res.indexedUsers, nameSelection)
            })
            .then(res => {
                console.log(res);
                return this.dispatchGrouping(res.alphaUsers)
            })
            .then(res => {
                console.log(res);
                const userString = this.state.usersName;
                const nameSelection = this.state.nameSearchSelection;
                return this.dispatchFilterUsers(res.groupedUsers, userString, nameSelection)
            })
            .then(res => {
                console.log(res);
                return this.dispatchAlphabetizeGroups(res.filteredUsers)
            })
            .then(res => {
                console.log(res);
                this.setState({filteredList: res.alphaGroups});
            })

    }

    handleUserNumberChange = (event, index, value) => {
        this.setState({numberOfUsers: value})
    };

    getNewUsersList = () => {
        this.setState({usersName: ''});
        const userNumber = this.state.numberOfUsers;
        this.dispatchUsers(userNumber)
            .then(res => {
                console.log(res);
                const nameSelection = this.state.nameSearchSelection;
                return this.dispatchIndex(res.users, nameSelection);
            })
            .then(res => {
                console.log(res);
                const nameSelection = this.state.nameSearchSelection;
                return this.dispatchAlphabetizeUsers(res.indexedUsers, nameSelection)
            })
            .then(res => {
                console.log(res);
                return this.dispatchGrouping(res.alphaUsers)
            })
            .then(res => {
                console.log(res);
                const userString = this.state.usersName;
                const nameSelection = this.state.nameSearchSelection;
                return this.dispatchFilterUsers(res.groupedUsers, userString, nameSelection)
            })
            .then(res => {
                console.log(res);
                return this.dispatchAlphabetizeGroups(res.filteredUsers)
            })
            .then(res => {
                console.log(res);
                this.setState({filteredList: res.alphaGroups});
            })
    };

    sortUserList = (event, usersName) => {
        this.setState({usersName: usersName});
        const userList = this.state.userList;
        const nameSelection = this.state.nameSearchSelection;

        this.setState({usersName: usersName});

        this.dispatchFilterUsers(userList, usersName, nameSelection)
            .then(res => {
                return this.dispatchIndex(res.filteredUsers, nameSelection);
            })
            .then(res => {
                const nameSelection = this.state.nameSearchSelection;
                return this.dispatchAlphabetizeUsers(res.indexedUsers, nameSelection)
            })
            .then(res => {
                return this.dispatchGrouping(res.alphaUsers)
            })
            .then(res => {
                return this.dispatchAlphabetizeGroups(res.groupedUsers)
            })
            .then(res => {
                console.log(res);
                this.setState({filteredList: res.alphaGroups});
            })
            .catch(err => {
                return Promise.reject(err);
            });

    };

    handleLastChecked = () => {
        const nameSelection = 'last';
        const userList = this.state.userList;
        const usersName = this.state.usersName;
        this.setState({
            nameSearchSelection: nameSelection,
            firstChecked: false,
            lastChecked: true
        });
        this.dispatchFilterUsers(userList, usersName, nameSelection)
            .then(res => {
                return this.dispatchIndex(res.filteredUsers, nameSelection);
            })
            .then(res => {
                const nameSelection = this.state.nameSearchSelection;
                return this.dispatchAlphabetizeUsers(res.indexedUsers, nameSelection)
            })
            .then(res => {
                return this.dispatchGrouping(res.alphaUsers)
            })
            .then(res => {
                return this.dispatchAlphabetizeGroups(res.groupedUsers)
            })
            .then(res => {
                console.log(res);
                this.setState({filteredList: res.alphaGroups});
            })
            .catch(err => {
                return Promise.reject(err);
            });
    };
    handleFirstChecked = () => {
        const nameSelection = 'first';
        const userList = this.state.userList;
        const usersName = this.state.usersName;
        this.setState({
            nameSearchSelection: nameSelection,
            lastChecked: false,
            firstChecked: true
        });
        this.dispatchFilterUsers(userList, usersName, nameSelection)
            .then(res => {
                return this.dispatchIndex(res.filteredUsers, nameSelection);
            })
            .then(res => {
                const nameSelection = this.state.nameSearchSelection;
                return this.dispatchAlphabetizeUsers(res.indexedUsers, nameSelection)
            })
            .then(res => {
                return this.dispatchGrouping(res.alphaUsers)
            })
            .then(res => {
                return this.dispatchAlphabetizeGroups(res.groupedUsers)
            })
            .then(res => {
                console.log(res);
                this.setState({filteredList: res.alphaGroups});
            })
            .catch(err => {
                return Promise.reject(err);
            });
    };

    handleDialogOpen = () => {
        this.setState({profileOpen: true});
    };

    handleDialogClose = () => {
        this.setState({profileOpen: false});
    };

    render () {

        const actions = [
            <FlatButton
                label="Ok"
                primary={true}
                onTouchTap={this.handleDialogClose}
            />
        ];

        return (

           <div>
               <Card style={{marginLeft: '20%', marginRight: '20%'}}>
                   <CardText style={{margin: 'auto'}}>
                       <RaisedButton
                           label="Get new user list"
                           onTouchTap={this.getNewUsersList}
                       />
                       <DropDownMenu
                           value={this.state.numberOfUsers}
                           onChange={this.handleUserNumberChange}
                           style={{width: 'auto'}}
                           labelStyle={{top: 20}}
                           iconStyle={{top: 25}}
                           underlineStyle={{top: 65}}
                       >
                           <MenuItem value={50} primaryText="50"/>
                           <MenuItem value={75} primaryText="75"/>
                           <MenuItem value={100} primaryText="100"/>
                           <MenuItem value={125} primaryText="125"/>
                           <MenuItem value={150} primaryText="150"/>
                       </DropDownMenu>
                       <TextField
                           id="userSearchField"
                           hintText="User Search"
                           floatingLabelText="User Search"
                           value={this.state.usersName}
                           onChange={this.sortUserList}
                           style={{marginLeft: 50}}
                           autoFocus
                       />
                   </CardText>
                   <CardText style={{width: 'auto'}}>
                       <CardHeader
                           title="Sort By"
                           style={{padding: 5}}
                       />
                      <Checkbox
                        label="Last Name"
                        checked={this.state.lastChecked}
                        onCheck={this.handleLastChecked}
                      />
                       <Checkbox
                           label="First Name"
                           checked={this.state.firstChecked}
                           onCheck={this.handleFirstChecked}
                       />
                   </CardText>
               </Card>

               {this.state.filteredList.map((user, i) =>
                   <CardText expandable={true} key={i} style={{marginLeft: '20%', marginRight: '20%'}}>
                       <CardHeader
                            title={user.group}
                       />
                       <Divider style={{height: 10}}/>
                       <GridList cellHeight={'auto'} cols={3}>

                           {user.groupedUsers.map((user, i) =>
                               <GridTile
                                   key={i}
                                   title={user.name.last + ', ' + user.name.first}
                                   subtitle={user.dob}
                                   style={{height: 250, width: 200}}
                                   onTouchTap={this.handleDialogOpen}
                               >
                                   <img src={user.picture.large} style={{height: 250, width: 200}}/>

                                   <Dialog
                                       title={user.name.last + ', ' + user.name.first}
                                       actions={actions}
                                       modal={false}
                                       autoDetectWindowHeight={true}
                                       autoScrollBodyContent={true}
                                       open={this.state.profileOpen}
                                       onRequestClose={this.handleDialogClose}
                                   >
                                       <img src={user.picture.large} style={{height: 250, width: 200}}/>
                                       <CardHeader
                                           title={user.email}
                                           style={{padding: 5}}
                                       />
                                       <CardHeader
                                           title={user.cell}
                                           style={{padding: 5}}
                                       />
                                       <CardHeader
                                           title={user.phone}
                                           style={{padding: 5}}
                                       />
                                       <CardHeader
                                           title={user.location.street}
                                           style={{padding: 5}}
                                       />
                                       <CardHeader
                                           title={user.location.city}
                                           style={{padding: 5}}
                                       />
                                       <CardHeader
                                           title={user.location.state}
                                           style={{padding: 5}}
                                       />
                                   </Dialog>
                               </GridTile>
                           )}
                       </GridList>
                   </CardText>
               )}
           </div>

        )
    }
}

