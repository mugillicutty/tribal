
import 'babel-polyfill';
import React from 'react'
import ReactDOM from 'react-dom'
import { Router, browserHistory } from 'react-router'
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();
import Root from './routes';

import Store from './MyStore';

ReactDOM.render(
    <Root store={Store} history={browserHistory}/>,
    document.getElementById('app')
);