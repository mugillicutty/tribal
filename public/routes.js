
import React from 'react'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import {Provider} from 'react-redux';

import UserSortPage from './pages/UserSortPage';

const Root = ({ store }) => (
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component={UserSortPage} />
        </Router>
    </Provider>
);

export default Root;
