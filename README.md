# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Sorting application for Tribalquest.


### How do I get set up? ###

* If running om Mac:
* You will need to make sure you have node v6.10.2 and npm installed globally.
* It will also help if you have the process manager pm2 installed globally as well.

### Next ###

* Select a location on your computer to clone into the repository.
* Once you have open a terminal window and go to that directory using "cd /path/to/directory".
* Then clone into the repository using this command "git clone https://mugillicutty@bitbucket.org/mugillicutty/tribal.git".
* When the repository has finished cloning run the command "npm install" to install all the moduled needed to run the appplication.
* Lastly if everything has gone as expected run "npm run build:start" to build the application and start thr pm2 server.
* Then you should be able to visit http://localhost:7373/ in your browser and use the application.

### Cloud service ###

* Assuming you will be using a linux OS you will need to ssh into your instance and run the command "sudo apt-get build-essential". this will install some basic OS features for running the application
* You will then follow the instrunctions from "How do i get set up?" and "Next" but stop before building your application and starting the pm2 server.
* For the hosted service you will download nginx server to use as a proxy.
* Once downloaded navigate to /etc/nginx/sites-available/ and copy the filethere that is named "default" and name it "tribal".
* you will then edit the config file you just created and make sure it is listening on port 80 and at http://localhost:7373, and is stil labeled "default_server".
* create a server block and place the path to the index.html file above the list of files named the same and place inside the server block "proxy_pass http://localhost:7373, for any request looking for path "/".
* Assuming you would have a cert and damain you would place the domain in the losten line and the paths to the certs alond with SSL on and listen to port 443.
* Save the file and exit. Then run "sudo service nginx start" to start the nginx server.
* Afterwars go back to the directory you cloned the repository into and run "npm run build:start" to build the application and start thr pm2 server.
* Then you should be able to visit http://localhost:7373/ in your browser and use the application.

* Afterward you should be able to visit the application online.

### More time? ###

* I would like to fix the response times and dialog issue most.
* I would like to work on the look of the app, work on the experience and layout
* I would hav liked to add a feature such as typahead so users could see the person they were looking for below he search bar and go directly to them.
* I also would have liked a way to make the quality of the images better.
* I think a way to select and group people you are looking for to then compare or put into groups. A way to customize each users view of the random users for whatever purpose suited them.






